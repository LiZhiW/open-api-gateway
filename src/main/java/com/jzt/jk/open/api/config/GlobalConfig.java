package com.jzt.jk.open.api.config;

import lombok.Data;

/**
 * 作者：lizw <br/>
 * 创建时间：2021/01/26 10:20 <br/>
 */
//@ConfigurationProperties(prefix = GlobalConfig.ConfigPrefix)
@Data
public class GlobalConfig {
    public static final String ConfigPrefix = "open-api";
}
