package com.jzt.jk.open.api.service;

import com.jzt.jk.open.api.bo.ApiServiceConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 作者：lizw <br/>
 * 创建时间：2021/01/26 11:26 <br/>
 */
@Transactional(readOnly = true)
@Service
@Slf4j
public class OpenApiConfigCacheService {
    /**
     *
     */
    private final Map<String, ApiServiceConfig> apiServiceConfigMap = new ConcurrentHashMap<>();

    // ak sk path
    // ak sk -> ApiService
    // ak sk path -> ApiInfo

    public boolean canAccess(String appKey, String appSecret, String path, String method) {
        return true;
    }
}
