package com.jzt.jk.open.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jzt.jk.open.api.bo.OpenApiConfig;
import com.jzt.jk.open.api.entity.ApiInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 作者：lizw <br/>
 * 创建时间：2021/01/26 11:05 <br/>
 */
@Repository
@Mapper
public interface ApiInfoMapper extends BaseMapper<ApiInfo> {

    /**
     * 查询所以有效的API配置
     */
    List<OpenApiConfig> findAllOpenApiConfig();
}
