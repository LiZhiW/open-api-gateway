package com.jzt.jk.open.api.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 应用(Application)实体类
 *
 * @author lizw
 * @since 2021-01-26 11:04:31
 */
@Data
public class Application implements Serializable {
    private static final long serialVersionUID = 258189503755226775L;
    /**
     * 应用id
     */
    private Long id;

    /**
     * 应用名称
     */
    private String name;

    /**
     * 授权方式，1:内部应用，2:外部应用
     */
    private Integer authType;

    /**
     * 开发者-企业id
     */
    private Long devEnterpriseId;

    /**
     * 接入模式，1:API，2:H5
     */
    private Integer openMode;

    /**
     * 状态，1:调试中，2:待审核，3:审核通过，4:审核驳回，5:已下线
     */
    private Integer state;

    /**
     * 应用描述
     */
    private String description;

    /**
     * 创建用户id
     */
    private String createUid;

    /**
     * 创建用户名
     */
    private String createUname;

    /**
     * 审核用户id
     */
    private String auditUid;

    /**
     * 审核用户名
     */
    private String auditUname;

    /**
     * 审核时间
     */
    private Date auditTime;

    /**
     * 审核消息(驳回原因)
     */
    private String auditMessage;

    /**
     * 生产环境app_key
     */
    private String prodAppKey;

    /**
     * 生产环境app_secret
     */
    private String prodAppSecret;

    /**
     * 测试环境app_key
     */
    private String testAppKey;

    /**
     * 测试环境app_secret
     */
    private String testAppSecret;

    /**
     * 开发环境app_key
     */
    private String devAppKey;

    /**
     * 开发环境app_secret
     */
    private String devAppSecret;

    /**
     * 创建用户id
     */
    private String createBy;

    /**
     * 更新用户id
     */
    private String updateBy;

    /**
     * 创建时间
     */
    private Date createAt;

    /**
     * 更新时间
     */
    private Date updateAt;

}