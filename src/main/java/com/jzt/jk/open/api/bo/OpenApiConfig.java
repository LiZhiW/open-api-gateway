package com.jzt.jk.open.api.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * 作者：lizw <br/>
 * 创建时间：2021/01/26 11:06 <br/>
 */
@Data
public class OpenApiConfig implements Serializable {
    // ------------------------------------------------------------------------------------------------------------------ Application
    /**
     * 应用id
     */
    private Long applicationId;
    /**
     * 应用名称
     */
    private String applicationName;
    /**
     * 授权方式，1:内部应用，2:外部应用
     */
    private Integer authType;
    /**
     * 生产环境app_key
     */
    private String prodAppKey;
    /**
     * 生产环境app_secret
     */
    private String prodAppSecret;
    /**
     * 测试环境app_key
     */
    private String testAppKey;
    /**
     * 测试环境app_secret
     */
    private String testAppSecret;
    /**
     * 开发环境app_key
     */
    private String devAppKey;
    /**
     * 开发环境app_secret
     */
    private String devAppSecret;

    // ------------------------------------------------------------------------------------------------------------------ ApiService
    /**
     * API服务id
     */
    private Long apiServiceId;
    /**
     * 服务名称
     */
    private String apiServiceName;
    /**
     * 生产环境地址
     */
    private String prodUri;
    /**
     * 测试环境地址
     */
    private String testUri;
    /**
     * 开发环境地址
     */
    private String devUri;
    /**
     * 服务配置(JSON格式数据)
     */
    private String config;
    /**
     * 配置排序(order)
     */
    private Integer sort;
    /**
     * Spring Cloud Gateway的路由定义配置
     */
    private String routeDefinition;

    // ------------------------------------------------------------------------------------------------------------------ ApiInfo
    /**
     * API id
     */
    private Long apiInfoId;
    /**
     * API名称
     */
    private String apiInfoName;
    /**
     * API Code
     */
    private String code;
    /**
     * API访问路径
     */
    private String path;
    /**
     * HTTP Method
     */
    private String method;
}
