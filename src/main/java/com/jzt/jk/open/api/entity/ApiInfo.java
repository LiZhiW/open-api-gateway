package com.jzt.jk.open.api.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * API信息(ApiInfo)实体类
 *
 * @author lizw
 * @since 2021-01-26 11:04:33
 */
@Data
public class ApiInfo implements Serializable {
    private static final long serialVersionUID = 864014368597487398L;
    /**
     * API id
     */
    private Long id;

    /**
     * API所属服务
     */
    private Long apiServiceId;

    /**
     * API Code
     */
    private String code;

    /**
     * API名称
     */
    private String name;

    /**
     * API访问路径
     */
    private String path;

    /**
     * HTTP Method
     */
    private String method;

    /**
     * 状态，1:已发布，2:已下线
     */
    private Integer state;

    /**
     * API文档
     */
    private String doc;

    /**
     * 说明
     */
    private String description;

    /**
     * 创建用户id
     */
    private String createBy;

    /**
     * 更新用户id
     */
    private String updateBy;

    /**
     * 创建时间
     */
    private Date createAt;

    /**
     * 更新时间
     */
    private Date updateAt;

}