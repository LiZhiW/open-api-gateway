package com.jzt.jk.open.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * 作者：lizw <br/>
 * 创建时间：2021/01/26 14:09 <br/>
 */
public class ForbiddenException extends ResponseStatusException {
    public ForbiddenException(String reason) {
        super(HttpStatus.FORBIDDEN, reason);
    }
}
