package com.jzt.jk.open.api.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * API服务(ApiService)实体类
 *
 * @author lizw
 * @since 2021-01-26 11:04:32
 */
@Data
public class ApiService implements Serializable {
    private static final long serialVersionUID = 208190740589970840L;
    /**
     * API服务id
     */
    private Long id;

    /**
     * 服务名称
     */
    private String name;

    /**
     * 生产环境地址
     */
    private String prodUri;

    /**
     * 测试环境地址
     */
    private String testUri;

    /**
     * 开发环境地址
     */
    private String devUri;

    /**
     * 服务配置(JSON格式数据)
     */
    private String config;

    /**
     * 配置排序(order)
     */
    private Integer sort;

    /**
     * Spring Cloud Gateway的路由定义配置
     */
    private String routeDefinition;

    /**
     * 说明
     */
    private String description;

    /**
     * 创建用户id
     */
    private String createBy;

    /**
     * 更新用户id
     */
    private String updateBy;

    /**
     * 创建时间
     */
    private Date createAt;

    /**
     * 更新时间
     */
    private Date updateAt;

}