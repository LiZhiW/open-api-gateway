package com.jzt.jk.open.api.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * 作者：lizw <br/>
 * 创建时间：2021/01/26 11:28 <br/>
 */
@Data
public class ApiInfoConfig implements Serializable {

}
