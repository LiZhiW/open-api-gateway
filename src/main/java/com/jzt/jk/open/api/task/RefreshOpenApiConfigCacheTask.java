package com.jzt.jk.open.api.task;

import com.jzt.jk.open.api.service.OpenApiConfigCacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 作者：lizw <br/>
 * 创建时间：2021/01/26 11:46 <br/>
 */
@Component
@Slf4j
public class RefreshOpenApiConfigCacheTask {
    @Autowired
    public OpenApiConfigCacheService openApiConfigCacheService;

    @Scheduled(initialDelay = 0, fixedDelay = 1000 * 8)
    public void refreshCache() {
        long startTime = System.currentTimeMillis();
        log.debug("[刷新OpenApiConfig] - 开始...");
        try {
//            serverAccessSupportService.reloadServerAccessToken();
        } catch (Exception e) {
            log.debug("[刷新OpenApiConfig] - 失败", e);
        } finally {
            long useTime = System.currentTimeMillis() - startTime;
            log.info("[刷新OpenApiConfig] - 完成，耗时: {}ms", useTime);
        }
    }
}
